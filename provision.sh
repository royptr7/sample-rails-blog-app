#!/usr/bin/env bash

# Install system dependency
is_installed=`which ruby`
if [ -z $is_installed]; then
  sudo apt-add-repository ppa:brightbox/ruby-ng -y
  sudo apt-get update
  sudo apt-get install ruby2.4 ruby2.4-dev -y
  sudo apt-get install libsqlite3-dev -y
  sudo apt-get install nodejs -y
  sudo gem install bundle
fi

# Get app
sudo apt-get install -y  wget unzip
sudo apt-get  install -y libpq-dev
sudo mkdir /opt/sample-rails-blog-app
sudo rm -rf /opt/sample-rails-blog-app/*
sudo chown -R vagrant:vagrant /opt/sample-rails-blog-app
cd /opt/sample-rails-blog-app
wget https://gitlab.com/royptr7/sample-rails-blog-app/-/jobs/56784122/artifacts/download -O sample-rails-blog-app.zip
unzip sample-rails-blog-app.zip
rm -rf sample-rails-blog-app.zip
bundle install --local

# Run the app
#sudo pkill -f "puma 3.11.3 (tcp://0.0.0.0:3000) [vagrant]"
#./run.sh

