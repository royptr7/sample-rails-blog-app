Rails.application.routes.draw do
  get 'welcome/index'  # Adds the url
	resources :articles do
		resources :comments
	end
  root 'welcome#index' # Links to controller 

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
